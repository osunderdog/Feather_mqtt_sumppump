#include "noiseSignal.hpp"


NoiseSignal::NoiseSignal ():
  _highEWMA(4),
  _middleEWMA(4),
  _lowEWMA(4)
{
  
}

NoiseSignal::NoiseSignal (uint8_t highFrac = 4,
			  uint8_t middleFrac = 4,
			  uint8_t lowFrac = 4):
  _highEWMA(highFrac),
  _middleEWMA(middleFrac),
  _lowEWMA(lowFrac)
{
  
}

uint16_t NoiseSignal::getHighValue(void)
{
  return _highEWMA.result();
}

uint16_t NoiseSignal::getMiddleValue(void)
{
  return _middleEWMA.result();
}

uint16_t NoiseSignal::getLowValue(void)
{
  return _lowEWMA.result();
}

uint16_t NoiseSignal::getNoise(void)
{
  return ABS_DELTA(_highEWMA, _lowEWMA);
}

void NoiseSignal::update(const uint16_t value)
{
  //keep a middle value
  _middleEWMA.update(value);
  
  //if value is greater than middle value then add it to the EWMA for the high value
  if(value > _middleEWMA)
  {
    _highEWMA.update(value);
  }
 
  if(value < _middleEWMA)
  {
    _lowEWMA.update(value);
  }  
}
