Feather_mqtt_sumppump

Hardware used in this setup:
Adafruit Feather ESP8266: SOC WiFi
Adafruit ADS1015: Analog to digital Converter
CT Sensor noninvasive: Sparkfun SEN-10341
Zener diodes for protection from spikes
Resistors to trim signal

Adafruit Feather with ADC1015.  Read Voltage from AC current sensor and report over MQTT.  
File missing SecureData.h contains defines that are not intended to be shared publicly. 

Working to provide an example of an IOT device that solves a real problem.
I've got a sump pump in the basement that cycles occasionally. I would like to know how 
frequently it cycles and how long each cycle runs.

Useful links:

Products
Adafruit Feather HUZZAH with ESP8266 WiFi:
https://www.adafruit.com/products/2821

ADS1015 12-Bit ADC - 4 Channel with Programmable Gain Amplifier:
https://www.adafruit.com/products/1083

SEN-11005: Non-Invasive Current Sensor - 30A:
https://www.sparkfun.com/products/11005

Research:
Open Energy web site has some good information on home sensing:
https://openenergymonitor.org/

Great tutorial on zener diodes that helped me do spike protection:
http://www.evilmadscientist.com/2012/basics-introduction-to-zener-diodes/

Information on MQTT protocol:
https://learn.adafruit.com/adafruit-io/mqtt-api

Feather pinouts:
https://learn.adafruit.com/adafruit-feather-huzzah-esp8266/pinouts

