#ifndef noiseSignal_h
#define noiseSignal_h
#include <ESP8266WiFi.h>

#include <stdio.h>
#include "ewma.hpp"

#define ABS_DELTA(x,y) ( ((x > y)?(x - y):(y - x)) );

class NoiseSignal
{
private:
  EWMA _highEWMA;
  EWMA _middleEWMA;
  EWMA _lowEWMA;
  
public:
  NoiseSignal();
  NoiseSignal(uint8_t,
	      uint8_t,
	      uint8_t);

  void update(const uint16_t);

  uint16_t getHighValue(void);
  uint16_t getMiddleValue(void);
  uint16_t getLowValue(void);
  uint16_t getNoise(void);
};

#endif //#define noiseSignal_h
