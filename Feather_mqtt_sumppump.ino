/*
 SumpPump Detector.
 Written By Bruce W. Lowther

 Track voltage through ADC1015.
 Record voltage into ewma for
 Middle, High and Low voltage.
 Report delta between low and high voltage to MQTT.
*/

/***************************************************
  Adafruit MQTT Library ESP8266 Example

  Must use ESP8266 Arduino from:
    https://github.com/esp8266/Arduino

  Works great with Adafruit's Huzzah ESP board & Feather
  ----> https://www.adafruit.com/product/2471
  ----> https://www.adafruit.com/products/2821

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Tony DiCola for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/
#include "Feather_mqtt_sumppump.h"
#include "noiseSignal.hpp"

/***/
Adafruit_ADS1015 ads;

/*
Declare a ewma values for oscilating voltage.
Track middle, high, low.  Noise is delta between high and low.
Noise is reported over MQTT.
*/

/*
*/
NoiseSignal ns(EWMA_ROUND,EWMA_ROUND,EWMA_ROUND);

/*
 * when mqttAllowReport is set to HIGH then report data over mqtt channel.
 * otherwise, do not report data but keep mqtt alive with pings.  Toggled 
 * by sumppupm_monitor_onoff mqtt subscription.
 * 
*/
uint8_t mqttReportData = HIGH;

//start out with define but could be adjusted by MQTT.
uint16_t mqttSumppumpNoiseThreshold = NOISE_THRESHOLD_HIGH;

//used to count number of adc samples to do before publishing.
uint8_t adc_sample_index = 0;

/***/


/************ Global State (you don't need to change this!) ******************/

// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;
// or... use WiFiFlientSecure for SSL
//WiFiClientSecure client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);

/****************************** Feeds ***************************************/

// Setup a feed called 'photocell' for publishing.
// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname>
// The syntaxt of these make me uneasy.  They don't look like valid C...
// I'm just sayin'...
//
//  Ok. here's the MQTT topic name that this is publishing to:
//  osunderdog/feeds/sumppump_voltage
//
//  so somewhere osunderdog "/feeds/sumppump_voltage" is getting translated to 
//  
Adafruit_MQTT_Publish sumppump_publish_voltage = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME MQTT_SUB_VOLTAGE);
Adafruit_MQTT_Publish sumppump_publish_onoff = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME MQTT_SUB_ONOFF);

// Setup a feed called 'onoff' for subscribing to changes.
Adafruit_MQTT_Subscribe sumppump_monitor_onoff = Adafruit_MQTT_Subscribe(&mqtt, AIO_USERNAME MQTT_SUB_MONITOR_ONOFF);
Adafruit_MQTT_Subscribe sumppump_monitor_noiseThreshold = Adafruit_MQTT_Subscribe(&mqtt, AIO_USERNAME MQTT_SUB_MONITOR_THRESHOLD);


/*************************** Sketch Code ************************************/

// Bug workaround for Arduino 1.6.6, it seems to need a function declaration
// for some reason (only affects ESP8266, likely an arduino-builder bug).
void MQTT_connect();

void inline togglePIN(uint8_t pin) 
{
  digitalWrite(pin, !digitalRead(pin));
}


void setup() {

  Serial.end();
  //Serial.begin(115200);
  delay(10);

//LED setups.
  pinMode(R_LED, OUTPUT);
  pinMode(B_LED, OUTPUT);

  //Start voltage sensor.
  ads.begin();

  Serial.println(F("Adafruit MQTT sump pump"));

  // Connect to WiFi access point.
  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());

  // Setup MQTT subscription for onoff feed.
  mqtt.subscribe(&sumppump_monitor_onoff);
  mqtt.subscribe(&sumppump_monitor_noiseThreshold);
}

void loop() {
  //update our adc_sample_index ... sample as fast as possible, but only publish MQTT occasionally.
  adc_sample_index++;
  
  //always update the ADC... other stuff may happen optionally.

  digitalWrite(R_LED, HIGH);

  //ADC and EWMA calculation stuff.
  //blink led to indicate readings...
  ns.update(ads.readADC_SingleEnded(0));

  digitalWrite(R_LED, LOW);

  //occasionally check to see if the threshold has changed.  It shouldn't happen that often...
  //subscriptions are very expensive...
  if(MQTT_CHECK_SUBSCRIPTIONS & (adc_sample_index & (1 << 7)))
  {
    togglePIN(B_LED);
    togglePIN(B_LED);
  
    Serial.print("Checking Subscriptions...");

    MQTT_connect();
    // this is our 'wait for incoming subscription packets' busy subloop
    // try to spend your time here  
    Adafruit_MQTT_Subscribe *subscription;
    while ((subscription = mqtt.readSubscription(5000))) {
      if (subscription == &sumppump_monitor_onoff) {
        mqttReportData = ((strcmp((char *)sumppump_monitor_onoff.lastread,"ON"))?(LOW):(HIGH));
        }
        
      if (subscription == &sumppump_monitor_noiseThreshold) {
        Serial.print(F("threshold:"));
        Serial.println((char *)sumppump_monitor_noiseThreshold.lastread);
        mqttSumppumpNoiseThreshold = atol((char *)sumppump_monitor_noiseThreshold.lastread);
        }
      Serial.print('.');
    }
    Serial.println("...done");

    togglePIN(B_LED);
    togglePIN(B_LED);
  }

  //publish every so often.
  if(adc_sample_index & (1 << 6))
  {
    Serial.print("Publishing noise data...");
    //reset the time index... in case we are using a non zero reset time.
    adc_sample_index = 0;

    // Ensure the connection to the MQTT server is alive (this will make the first
    // connection and automatically reconnect when disconnected).  See the MQTT_connect
    // function definition further below.
    MQTT_connect();

    //if it's time to publish data then do so.  
    if(mqttReportData == HIGH)
    {
      Serial.print(F("noiseVolume:"));
      Serial.println(ns.getNoise());

      togglePIN(B_LED);
  
      //publish on/off state of sump pump.
      if (! sumppump_publish_onoff.publish(((ns.getNoise() > mqttSumppumpNoiseThreshold)?(HIGH):(LOW)))) {
        Serial.println(F("Failed"));
      }
  
      //publish voltage of sump pump.
      if (! sumppump_publish_voltage.publish(ns.getNoise())) {
        Serial.println(F("Failed"));
      }
      togglePIN(B_LED);
    }
    else
    {
      Serial.print("just ping [");
      Serial.print(mqttReportData);
      Serial.print("]\n");
      // ping the server to keep the mqtt connection alive
      // NOT required if you are publishing once every KEEPALIVE seconds
      if(! mqtt.ping()) {
        mqtt.disconnect();
      }
    Serial.print("Publishing noise data...");
    }
  }
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}


