#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"

#include <Adafruit_ADS1015.h>
#include "SecureData.h"

/************************* WiFi Access Point ********************************
#define SECURE_DATA_WIFI
#define WLAN_SSID       "{YOUR NETWORK SSID HERE}"
#define WLAN_PASS       "{YOUR NETWORK PASSWORD}"
*/
#ifndef SECURE_DATA_WIFI
#error You must define WiFi Connection information.
#endif


/************************* Adafruit.io Setup ********************************
#define SECURE_DATA_MQTT
#define AIO_SERVER      "{}"
#define AIO_SERVERPORT  9999                   // use 8883 for SSL
#define AIO_USERNAME    "{USERNAME}"
#define AIO_KEY         "{KEY}"
*/

#ifndef SECURE_DATA_MQTT
#error You must define MQTT Connection information.
#endif

//sensitivity.  Greater number, more noise spikes.
#define EWMA_ROUND 3

// Used to identify a legitimate noise. trigger action above high
#define NOISE_THRESHOLD_HIGH 300

// checking for subscriptions is very expensive... disabling until I can work out a better model.
#define MQTT_CHECK_SUBSCRIPTIONS LOW

//MQTT Addresses where data will be published.
#define MQTT_SUB_VOLTAGE "/feeds/sumppump_voltage"
#define MQTT_SUB_ONOFF   "/feeds/sumppump_onoff"

//MQTT Addresses monitored for values that will change behavior.
#define MQTT_SUB_MONITOR_THRESHOLD "/feeds/sumppump_threshold" 
#define MQTT_SUB_MONITOR_ONOFF     "/feeds/sumppump_monitor_onoff"


//LED indicators
#define S_LED 12   /* Indications */
#define Y_LED 14   /* */
#define G_LED 13   /* */
#define A_LED 15   /* Indications */
#define R_LED 0 /*GPIO #0 is on board red led.*/
#define B_LED 2 /*Onboard blue led*/


